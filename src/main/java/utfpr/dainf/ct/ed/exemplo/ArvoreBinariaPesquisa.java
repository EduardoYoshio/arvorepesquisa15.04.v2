package utfpr.dainf.ct.ed.exemplo;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * Exemplo de implementação de árvore binária de pesquisa.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 * @param <E> O tipo do valor armazenado nos nós da árvore
 */
public class ArvoreBinariaPesquisa<E extends Comparable<E>> extends ArvoreBinaria<E> {
    protected ArvoreBinariaPesquisa<E> pai;

    /**
     * Cria uma árvore com valor nulo na raiz.
     */
    public ArvoreBinariaPesquisa() {
    }

    /**
     * Cria uma árvore com o valor especificado na raiz.
     * @param valor O valor armazenado na raiz.
     */
    public ArvoreBinariaPesquisa(E valor) {
        super(valor);
    }

    /**
     * Inicializa o nó pai deste nó.
     * @param pai O nó pai.
     */
    protected void setPai(ArvoreBinariaPesquisa<E> pai) {
        this.pai = pai;
    }

    /**
     * Retorna o nó pai deste nó.
     * @return O nó pai.
     */
    protected ArvoreBinariaPesquisa<E> getPai() {
        return pai;
    }

    /**
     * Retorna o nó da árvore cujo valor corresponde ao especificado.
     * @param valor O valor a ser localizado.
     * @return A raiz da subárvore contendo o valor ou {@code null}.
     */
    public ArvoreBinariaPesquisa<E> pesquisa(E valor) {
    	ArvoreBinariaPesquisa<E> node = this;
    	E val = valor;
    	E valNo = node.valor;
        int comp = valor.compareTo(valNo);
    	while(comp != 0)
    	{
    		if (comp < 0)
    		{
    			node = (ArvoreBinariaPesquisa<E>)node.esquerda;
    			if(node != null)
    				valNo = node.valor;
    			else 
    				break;
    		}
    		else if(comp > 0)
    		{
    			node = (ArvoreBinariaPesquisa<E>)node.direita;
    			if(node != null)
    				valNo = node.valor;
    			else 
    				break;
    		}
            comp = valor.compareTo(valNo);
    	}
    	return node;
    }

    /**
     * Retorna o nó da árvore com o menor valor.
     * @return A raiz da subárvore contendo o valor mínimo
     */
    public ArvoreBinariaPesquisa<E> getMinimo() {
        ArvoreBinaria<E> node = this;
        while(node.esquerda != null)
        	node = node.esquerda;
        return (ArvoreBinariaPesquisa<E>) node;
    }

    /**
     * Retorna o nó da árvore com o maior valor.
     * @return A raiz da subárvore contendo o valor máximo
     */
    public ArvoreBinariaPesquisa<E> getMaximo() {
    	ArvoreBinaria<E> node = this;
        while(node.direita != null)
        	node = node.direita;
        return (ArvoreBinariaPesquisa<E>) node;
    }

    /**
     * Retorna o nó sucessor do nó especificado.
     * @param no O nó cujo sucessor desejamos localizar
     * @return O sucessor do no ou {@null}.
     */
    public ArvoreBinariaPesquisa<E> sucessor(ArvoreBinariaPesquisa<E> no) {
        ArvoreBinariaPesquisa<E> node = (ArvoreBinariaPesquisa<E>)no.direita;
        if(node != null)
        	while(node.esquerda != null)
        		node = (ArvoreBinariaPesquisa<E>)node.esquerda;
        else
        {
        	node = no.pai;
        	E val = no.getValor();
        	while(node != null) 
        	{
        		E valNode = node.getValor();
        		//if(valNode > val)
                if(val.compareTo(valNode) < 0)
        			return node;
        		else
        			node = node.pai;
        	}
        }
        return node;
    }

    /**
     * Retorna o nó predecessor do nó especificado.
     * @param no O nó cujo predecessor desejamos localizar
     * @return O predecessor do nó ou {@null}.
     */
    public ArvoreBinariaPesquisa<E> predecessor(ArvoreBinariaPesquisa<E> no) {
    	ArvoreBinariaPesquisa<E> node = (ArvoreBinariaPesquisa<E>)no.esquerda;
        if(node != null)
        	while(node.direita != null)
        		node = (ArvoreBinariaPesquisa<E>)node.direita;
        else
        {
        	node = no.pai;
        	E val = no.getValor();
        	while(node != null) 
        	{
        		E valNo = node.getValor();
        		//if(valNode < val)
                        if(val.compareTo(valNo) > 0)
        			return node;
        		else
        			node = node.pai;
        	}
        }
        return node;
    }

    /**
     * Insere um nó contendo o valor especificado na árvore.
     * @param valor O valor armazenado no nó.
     * @return O nó inserido
     */
    public ArvoreBinariaPesquisa<E> insere(E valor) {
    	ArvoreBinariaPesquisa<E> node = this;
    	E val = valor;
    	E valNo = node.valor;
    	//while(valNo != val)
        while(val.compareTo(valNo) != 0)
    	{
            //if(val > valNo)
            if(val.compareTo(valNo) > 0)
    		{
    			if(node.direita != null)
    			{
    				node = (ArvoreBinariaPesquisa<E>) node.direita;
    				valNo = node.valor;
    			}
    			else
    			{
    				ArvoreBinariaPesquisa<E> noIns = new ArvoreBinariaPesquisa<E>(valor);
    				noIns.setPai(node);
    				node.setDireita((ArvoreBinaria<E>)noIns);
    				return noIns;
    			}
    		} 
            //if(val < valNo)
    		else if(val.compareTo(valNo) < 0)
    		{
    			if(node.esquerda != null)
    			{
    				node = (ArvoreBinariaPesquisa<E>) node.esquerda;
    				valNo = node.valor;
    			}
    			else
    			{
    				ArvoreBinariaPesquisa<E> noIns = new ArvoreBinariaPesquisa<E>(valor);
    				noIns.setPai(node);
    				node.setEsquerda((ArvoreBinaria<E>)noIns);
    				return noIns;
    			}
    		}
    	}
    	return node;
    }

    /**
     * Exclui o nó especificado da árvore.
     * Se a raiz for excluída, retorna a nova raiz.
     * @param no O nó a ser excluído.
     * @return A raiz da árvore
     */
    public ArvoreBinariaPesquisa<E> exclui(ArvoreBinariaPesquisa<E> no) {
        no = pesquisa(no.getValor());        
        if(no.direita == null && no.esquerda == null)
        	return excluiNoSemFilho(no);
        else if(no.direita != null && no.esquerda == null)
        	return excluiNoUmFilho(no, (ArvoreBinariaPesquisa<E>)no.direita);
        else if(no.direita == null && no.esquerda != null)
        	return excluiNoUmFilho(no, (ArvoreBinariaPesquisa<E>)no.esquerda);
        else
        	return excluiNoDoisFilhos(no, (ArvoreBinariaPesquisa<E>)no.direita, (ArvoreBinariaPesquisa<E>)no.esquerda);
    }
    
    public ArvoreBinariaPesquisa<E> excluiNoSemFilho(ArvoreBinariaPesquisa<E> no) 
    {
    	ArvoreBinariaPesquisa<E> node = no.pai;
    	if(node == null)
    	{
    		/*
    		 *	NÃO SEI O QUE RETORNAR QUANDO EXCLUI A RAIZ E HÁ APENAS ELA
    		 */
                 no.setValor(null);
                 return this;
    	}
        else if(no == (ArvoreBinariaPesquisa<E>)node.esquerda)
        	node.esquerda = null;
        else
        	node.direita = null;
         
        return this;
    }
    
    public ArvoreBinariaPesquisa<E> excluiNoUmFilho(ArvoreBinariaPesquisa<E> no, ArvoreBinariaPesquisa<E> noFilho) 
    {
    	ArvoreBinariaPesquisa<E> node = no.pai;
    	if(node != null)
    	{
    		noFilho.setPai(node);
    		if((ArvoreBinariaPesquisa<E>)node.direita == no)
    		{
    			node.setDireita(noFilho);
    		}
    		else
    		{
    			node.setEsquerda(noFilho);
    		}	
    	}
    	else
    	{
    		noFilho.setPai(null);    		
    		return noFilho;
    	}
    	return this;  
    }
    
    
    public ArvoreBinariaPesquisa<E> excluiNoDoisFilhos(ArvoreBinariaPesquisa<E> no, 
    		ArvoreBinariaPesquisa<E> noFilhoD, ArvoreBinariaPesquisa<E> noFilhoE) 
    {
    	
    	ArvoreBinariaPesquisa<E> aux = this.sucessor(no);
	ArvoreBinariaPesquisa<E> auxFilho = (ArvoreBinariaPesquisa<E>)aux.direita;
	ArvoreBinariaPesquisa<E> auxPai = aux.pai;
	no.setValor(aux.getValor());
        
        // caso o sucessor seja o próprio filho da direita, 
        if(aux == noFilhoD)
        {
            no.setDireita(auxFilho);
            if(auxFilho != null)
                auxFilho.setPai(no);
        }
        else
        {
            // caso geral
            if(aux == auxPai.direita)
                auxPai.setDireita(auxFilho);
            else
                auxPai.setEsquerda(auxFilho);
            
            if(auxFilho != null)
                auxFilho.setPai(auxPai);
         
        }
        aux.setPai(null);
        aux.setDireita(null);
        aux.setEsquerda(null);
        
        return this;
    }
}
